! Lab 4, task 3

program task3
    implicit none
    integer :: i1
    integer, parameter :: N=5
    real(kind=8) :: sum

    sum = 0.d0
!Add a do-loop which sums the first N odd integers and prints the result
do i1 = 1,N,2
	sum = sum + i1

end do

print *, 'sum =', sum 

end program task3

! To compile this code:
! $ gfortran -o task3.exe lab4.3.f90
! To run the resulting executable: $ ./task3.exe