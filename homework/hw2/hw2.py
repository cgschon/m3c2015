"""Christopher Schon
00734692
"""


#import modules here
import numpy as np 
import matplotlib.pyplot as plt
import numpy.random as npr

#---------------------
def test_randn(n,m = 0,s = 1):
    """Input variables:
    n   number of normal random variables to be generated
    m   random variables generated with mean = m
    s   random variables generated with standard deviation = s
    """
    test = m + np.sqrt(s)*npr.randn(n)
    plt.figure()
    plt.hist(test)
    plt.xlabel('Value')
    plt.ylabel('Frequency')
    plt.title('test_randn, Christopher Schon')
 
#---------------------
def wiener1(dt,X0,Nt):
    """ Input variables:
    dt    time step
    X0    intial value, X(t=0) = X0
    Nt    number of time steps 
    """
    time_range = np.arange(0,(Nt+1)*dt,dt)

    rand_normals = np.sqrt(dt)*npr.randn(Nt)
    result = np.cumsum(np.insert(rand_normals, 0, X0))
    plt.figure()
    plt.xlabel('t')
    plt.title('wiener1, Chrisopher Schon with dt = 0.1, X0 = 0, Nt = 1000')
    plt.plot(result, label = r'$X(t)$')
    plt.plot(np.sqrt(time_range), color = 'plum', linestyle = '--', label = r'$\sigma$')
    plt.plot(-np.sqrt(time_range), color = 'plum', linestyle = '--')
    plt.legend(loc = 'upper left')






    
#---------------------
def wienerM(dt,X0,Nt,M):
    """ Input variables:
    dt    time step
    X0    intial value, X(t=0) = X0
    Nt    number of time steps 
    M     number of samples
    """
    time_range = np.arange(0,(Nt+1)*dt,dt)
    rand_normals_mat = np.insert(np.sqrt(dt)*npr.randn(M, Nt),0,X0, axis = 1)

    cum_rand_normals_mat = np.cumsum(rand_normals_mat, axis = 1)

    mean_result = np.mean(cum_rand_normals_mat, axis = 0)
    std_result = np.std(cum_rand_normals_mat, axis = 0)
    plt.figure()    
    plt.plot(time_range, mean_result, label = 'Mean')
    plt.plot(time_range, std_result, label = 'Standard Deviation')

    plt.plot(time_range, np.sqrt(time_range), color = 'plum', linestyle = '--', label = r'$\sigma$')
    plt.plot(time_range, -np.sqrt(time_range), color = 'plum', linestyle = '--')
    plt.axhline(X0, color = 'y', linestyle = '--', label = r'$\mu$')

    plt.legend(loc = 'upper left')
    plt.xlabel(r'$t$')
    plt.title('WienerM, Christopher Schon with dt = 0.1, X0 = 2.0, Nt = 1000, M = 20000')


#-----------------------------
def langevin1(dt,V0,Nt,g=1.0):
    """ Input variables:
    dt    time step
    V0    initial value, V(t=0) = V0
    Nt    number of time steps 
    g     friction factor, gamma
    """
    time_range = np.arange(0,(Nt+1)*dt, dt)
    langevin1_soln = np.insert(np.sqrt(dt)*npr.randn(Nt), 0, V0)
    for i in range(1,Nt+1):
        langevin1_soln[i] += langevin1_soln[i-1] - g*langevin1_soln[i-1]*dt 

    plt.figure()
    plt.plot(time_range, langevin1_soln, color = 'b', label = r"$V(t)$")
    plt.title('Langevin1, Christopher Schon with dt = 0.1, V0 = 0.0, Nt = 1000, g = 0.01')
    #plt.legend(loc = 'upper left')
    plt.ylabel(r'$V(t)$')
    plt.xlabel(r'$t$')
    


#-----------------------------
def langevinM(dt,V0,Nt,M,g=1.0,debug=False):
    """ Input variables:
    dt    time step
    V0    initial value, V(t=0) = V0
    Nt    number of time steps 
    M     number of samples
    g     friction factor, gamma
    debug flag to control plotting of figures
    """ 
    time_range = np.arange(0,(Nt+1)*dt, dt)
    langevinM_soln = np.insert(np.sqrt(dt)*npr.randn(M,Nt), 0, V0, axis = 1)

    for i in range(1,Nt+1):
        langevinM_soln[:,i] += langevinM_soln[:,i-1] - g*langevinM_soln[:,i-1]*dt 

    langevinM_soln_mean = np.mean(langevinM_soln, axis = 0)
    langevinM_soln_std = np.std(langevinM_soln, axis = 0)
    langevinM_soln_var = langevinM_soln_std*langevinM_soln_std

    analytical_mean = V0*np.exp(-(g*time_range))
    analytical_var = (1./(2*g))*(1-np.exp(-2*g*time_range))

    error = (1./Nt)*np.sum(np.abs(langevinM_soln_mean-analytical_mean))

    if debug:
        plt.figure()
        plt.plot(time_range, langevinM_soln_mean, label = r'$\bar V(t)$')
        plt.plot(time_range, langevinM_soln_std*langevinM_soln_std, label = 'Variance')
        plt.plot(time_range, analytical_mean, linestyle = '--', label = r'$\mu$')
        plt.plot(time_range, analytical_var, color = 'plum', linestyle = '--', label = r'$\sigma^2$')
        plt.legend(loc = 'upper left')
        plt.title('langevinM, Christopher Schon with dt = 0.1, V0 = 1.0, Nt = 1000, M = 2000, g = 0.05')
        plt.xlabel(r'$t$')
       


    return error







#--------------------------------
def langevinTest(dt,V0,Nt,g=1.0):
    """ Input variables:
    dt    time step
    V0    intial value, V(t=0) = V0
    Nt    number of time steps 
    g     friction factor, gamma
    """
    langevinM_errors = np.array(np.zeros(4))
    M = np.array([10, 100, 1000, 10000])
    for i in range(4):
        langevinM_errors[i] = langevinM(0.1,1.0,1000,M[i],0.05,False)


    plt.figure()
    plt.loglog(M, langevinM_errors)
    plt.title('langevinTest, Christopher Schon with dt = 0.1, V0 = 1.0, Nt = 1000, g = 0.25')
    plt.xlabel('M')
    plt.ylabel(r'$\epsilon$')

    best_poly = np.polyfit(np.log(M), np.log(langevinM_errors), 1)



    return -best_poly[0] 


#---------------------------------
#The section below is included for assessment and *must* be included exactly as below in the final
#file that you submit, but you may want to omit it (e.g. comment it out) while you are developing
#your code.  
if __name__ == "__main__":
    npr.seed(1)
    test_randn(1000)
    wiener1(0.1,0.0,1000)
    wienerM(0.1,2.0,1000,20000)
    langevin1(0.1,0.0,1000,0.01)
    print "Error from LangevinM, dt=%1.2f,V0=%1.2f,Nt=%d,M=%d,g=%1.3f: "%(0.1,1.0,1000,2000,0.05), langevinM(0.1,1.0,1000,2000,0.05,True)
    print "Convergence rate for Langevin Monte Carlo calculations from langevinTest", langevinTest(0.1,1.0,1000,0.25)
    plt.show()