"""
Assumes fdmodule
f2py --f90flags='-fopenmp' -lgomp -c fdmodule.f90 hw41_dev.f90 -m hw4
"""
from hw4 import fdmodule as f1
from hw4 import fdmodule2d as f2d
import numpy as np
import matplotlib.pyplot as plt


def test_grad1(n1,n2,numthreads):
    """obtain errors and timing values from test_grad and test_grad_omp
    return the two errors and average times from ten calls to the test routines
    Input: n1: number of points in x1
           n2: number of points in x2
           numthreads: number of threads used in test_grad_omp
    """
    
   
        
def test_gradN(numthreads):
    """input: number of threads used in test_grad_omp"""
    
    plt.savefig("hw421.png")


#This section will be used for assessment, you may alter the call to test_gradN if desired, but
#the other lines should be left as is in your final submission.    
if __name__ == "__main__":
    
    e,ep,t,tp = test_grad1(400,200,2)
    test_gradN(2) #modify if anything is being returned
