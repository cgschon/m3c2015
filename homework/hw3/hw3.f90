!Module containing routines for differentiating array of size N with
!2nd order and 4th order compact finite differences
!Test functions apply these methods to a Gaussian function and
!return performance information.

module fdmodule
    implicit none
    integer :: n
    real(kind=8) :: dx
    

    save

contains
!-------------------
subroutine fd2(f,df)
    !2nd order centered finite difference
    !Header variables
    !f: function to be differentiated
    !df: derivative of f
    implicit none
    real(kind=8), dimension(:), intent(in) :: f
    real(kind=8), dimension(size(f)), intent(out) :: df
    integer :: i
    
    
    
    df(1) = (1.0/(2.0*dx))*(f(2) - f(n))
    df(n) = (1.0/(2.0*dx))*(f(1) - f(n-1))
    
    do i = 2, size(f)-1
        df(i) = (1.0/(2.0*dx)) * (f(i+1) - f(i-1))
    end do
    
    
   

end subroutine fd2


    
    

subroutine cfd4(f,df)
    !compact 4th order centered finite difference
    !Header variables
    !f: function to be differentiated
    !df: derivative of f
    implicit none
    real(kind=8), dimension(:), intent(in) :: f
    real(kind=8), dimension(size(f)), intent(out) :: df
    integer :: M, NRHS, LDB, INFO, i
    double precision, dimension(n) :: D, B, BTemp
    double precision, dimension(n-1) :: DU, DL
    
    
    M = n
    NRHS = 1
    
    D(1) = 1
    D(M) = 1
    do i = 2,M-1
        D(i) = 4
    end do
    
    do i = 1,M-2
        DL(i) = 1
    end do
    
    DL(M-1) = 2
    DU(1) = 2
    do i = 2,M-1
        DU(i) = 1
    end do
    
    
    B(1) = (1.0/dx)*(-2.5*f(1) + 2.0*f(2) + 0.5*f(3))
    B(M) = (1.0/dx)*(2.5*f(M) -2.0*f(M-1) - 0.5*f(M-2))
    
    do i = 2,M-1
        B(i) = (1.0/dx)*3*(f(i+1) - f(i-1))
    end do
    
    LDB = B(1)
    
    !print *, 'DL =', DL
    !print *, 'D =', D
    !print *, 'DU =', DU
    !print *, 'B =', B
    
    
    
    call dgtsv(M,NRHS,DL,D,DU,B,M,INFO)
    !print *, 'INFO =', INFO
    
    !print *, 'Output =', B
    
    df = B
    !print *, 'cfd4 df = ', df
    
end subroutine cfd4
    
    
    
    
    

!------------------

subroutine test_fd(alpha,error)
    !test accuracy of fd2 and cfd4 with a Gaussian function, f=exp(-alpha*(x-x0)**2)
    !Header variables:
    !alpha: sets width of Gaussian
    !error: 2-element array containing fd2 and cfd4 approximation errors
    implicit none
    integer :: i
    real(kind=8), intent(in) :: alpha
    real(kind=8), intent(out) :: error(2)
    real(kind=8), dimension(n) :: x
    real(kind=8), dimension(n) :: fg
    real(kind=8), dimension(n) :: dfg
    real(kind=8), dimension(n) :: df2, cfd4df
    real(kind=8) :: x0 
    real(kind=8) :: lerror 
    
    do i = 1,n        !set analytical results 
        x(i) = REAL(i-1,8)*dx
    end do
        
    x0 = 0.5*MAXVAL(x)
        
    !print *, 'x =', x
    !print *, 'x0 =', x0
    
    do i = 1,n
        fg(i) = EXP(-alpha*(x(i) -x0)*(x(i)-x0))  
    end do
        
    do i = 1,n
        dfg(i) = -2.0*alpha*( x(i) - x0 )*fg(i)
    end do  
    
    !print *, 'fg =', fg
    !print *, 'dfg =', dfg
    
    call fd2(fg,df2) ! call fd2 giving derivative result in df2
    
    do i = 1,n
        lerror = lerror + (1.0/dble(n))*ABS(dfg(i) - df2(i)) !get error for fd2   
    end do
    
    
    
    error(1) = lerror ! put that error into error(1)
   
    lerror = 0 !reset lerror
    
    
    call cfd4(fg,cfd4df) !call cfd4 with cfd4df
    
   
    do i = 1,n
        lerror = lerror + (1.0/dble(n))*ABS(dfg(i) - cfd4df(i)) ! get error for fd2     
    end do
    
    !print *, 'cfd4 error =', lerror
    
    error(2) = lerror !put that error into error(2)
    
    
end subroutine test_fd
!---------------------

subroutine test_fd_time(alpha,error,time)
    !test accuracy and speed of fd2 and cfd4 with a Gaussian function, f=exp(-alpha*(x-x0)**2)
    !Header variables:
    !alpha: sets width of Gaussian
    !error: 2-element array containing fd2 and cfd4 approximation errors
    !time: 2-element array containing wall-clock time required by 1000 calls
    !to fd2 and cfd4
    implicit none
    real(kind=8), intent(in) :: alpha
    real(kind=8), intent(out) :: error(2),time(2)
    real(kind=8), dimension(n) :: fg, df, dfg
    integer(kind=8) :: i, clock1, clock2, clock_rate
    real(kind=8) :: lerror
    real(kind=8), dimension(n) :: x
    real(kind=8) :: x0
    
    do i = 1,n        !set analytical results 
        x(i) = REAL(i-1,8)*dx
    end do
        
    x0 = 0.5*MAXVAL(x)
    
    do i = 1,n
        fg(i) = EXP(-alpha*(x(i) -x0)*(x(i)-x0))   !set true fg
    end do
        
    do i = 1,n
        dfg(i) = -2.0*alpha*( x(i) - x0 )*fg(i) !set true dfg
    end do 
    
    call system_clock(clock1) !start timer
    do i = 1,1000
    call fd2(fg,df)
    end do
    call system_clock(clock2, clock_rate) ! end timer
    time(1) = dble(clock2-clock1)/dble(clock_rate) ! set time
    
    
    do i = 1,n
        lerror = lerror + (1.0/dble(n))*ABS(dfg(i) - df(i)) ! get error for fd2   
    end do
    
    error(1) = lerror
    
    lerror = 0
    
    call system_clock(clock1)
    

    do i = 1,1000
    call cfd4(fg,df)
    end do
    call system_clock(clock2, clock_rate)
    time(2) = dble(clock2-clock1)/dble(clock_rate)
    
    do i = 1,n
        lerror = lerror + (1.0/dble(n))*ABS(dfg(i) - df(i)) ! get error for fd2    
    end do
    
    error(2) = lerror
    
    
    
    
    

    
    
        
     
end subroutine test_fd_time
!---------------------------

end module fdmodule














